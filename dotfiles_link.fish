# dotfiles_link - Link some dotfiles in a given repository locally
# Author: Samuel Roland
# Synopsis: TODO
# Examples: TODO # why
#           TODO # why
# License: MIT
# Notice: Copyright (c) 2025 Samuel Roland
# Source: https://codeberg.org/samuelroland/productivity/src/branch/main/HEIG/tools

set VERSION 1
function dotfiles_link
    set selection (find |grep -P -v "^\.\/\.git|^\.\$" | grep -Po "\.\/\K.*"| fzf)
    if test -z $selection
        echo "No selection, action canceled"
    end

    set link_path $HOME"/"$selection
    set base_path $PWD"/"$selection
    echo "Linking $base_path to $link_path"
    ln -s $base_path $link_path
end
