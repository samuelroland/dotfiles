# .bashrc
# WARNING: this file is old and unmaintained ! I migrated to Fish (see .config/fish/config.fish) and converted my configuration in the Fish format,
# this is just here unmaintained, some of my functions are old or may not work anymore.

# TODO: really useful now that kitty has a default shell to fish ?
# Start fish if this is an interactive shell
# https://serverfault.com/questions/146745/how-can-i-check-in-bash-if-a-shell-is-running-in-interactive-mode
# if [[ $- == *i* ]]
# then
# 	fish
# fi

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi
unset rc

# Add bin folder of Composer, Cargo, Go and others
PATH=$PATH:$HOME/.composer/vendor/bin/
PATH=$PATH:$HOME/.local/bin
PATH=$PATH:/home/sam/go/bin

# Remove history size
## Help:  https://unix.stackexchange.com/questions/17574/is-there-a-maximum-size-to-the-bash-history-file
unset HISTSIZE
unset HISTFILESIZE
shopt -s histappend

# Aliases
# Bash related alias - easily edit bash config, reload bash config, search in history
alias eb="nvim ~/.bashrc"
alias rb="source ~/.bashrc"
alias bh="history | fzf" # TODO: insert this output in current command

## Browsing aliases
# alias r=ranger
alias s=sudo
alias r=yazi
alias o="open ."
alias ls=eza # A better ls wi
alias l="ls"
alias ll="ls -l"
alias la="ls -la"
alias lar="ls -laR"
alias cls="clear"
alias rm='rm -i'
alias mv='mv -i'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
# Open in default application (Dolphin if just a folder) with xdg-open
alias op="xdg-open ."
alias open="xdg-open"

# Open any PDF in $HOME, do not read .gitignore, filter lines by word "cours"
# to only include relevant PDF, use fzf to select and open with xdg-open with quotes around the filename
alias cours='fd -e pdf --no-ignore . $HOME | grep cours | fzf | xargs -I _ xdg-open "_"'

## Create a new folder and jump inside
mk() { mkdir -p "$@" && cd "$@"; }

## Tree command but with Eza and depth of 2 by default
lstree() {
    if [ $# -eq 0 ]; then
        count=2
    else
        count="$1"
    fi
    eza -TL $count --git-ignore
}

# Editor aliases
alias n="nvim"
alias co="codium ."
alias testco='ping 1.1.1.1 -c4'
export GIT_EDITOR=nvim # so Gitui doesn't open nano...
export EDITOR=/usr/bin/nvim

# Fedora aliases
alias listkernels="rpm -qa | grep kernel-core-"
alias dnf=dnf5
alias sdi="sudo dnf install"
alias rsi="sudo btrfs f usage /"
## Misc tools
alias spp=spotify_player
alias dlsound="youtube-dl -x --audio-format mp3"

# DEV
## Git aliases
alias st='git status'
alias log="git log --graph --pretty=format:'%C(green)%ai %d %C(blue)%H %C(green)%cn %C(bold yellow)%s %C(green)%b'"
alias cma='git commit -a -m'
alias cm='git commit -m'
alias push='git push'
alias lst='git ls-tree -r HEAD'
alias pull="git pull"
alias g="gitui"
# Load the SSH private key as Gitui doesn't read .ssh/config
ssh-add ~/.ssh/id_rsa_git &>/dev/null
alias nrp="npm run prod"
alias gf="git pull"
alias gp="git push"
alias gitcancel="git reset HEAD~1"
# Customized git diff (gd) with fzf
# Help: https://medium.com/@GroundControl/better-git-diffs-with-fzf-89083739a9cb
alias gd='git diff --name-only | fzf -m --ansi --preview "git diff --color=always -- {-1}"'

## Show information about Git identity
gitid() {
    echo "Current identity is"
    echo "Email: $(git config user.email)"
    echo "Name: $(git config user.name)"
}

## Fix incorrect origin
fixor() {
    git config remote.origin.url $1
}

## C and C++ aliases - cmake build - cmake build and run, rm cmake temp files - xmake build
alias cb="cmake . -Bbuild && cmake --build build/ -j 8"
alias cbr="cmake . -Bbuild && cmake --build build/ -j 8 && run"
alias rmcmake="rm -rf build/ CMakeFiles/ _deps/ CMakeCache.txt cmake_install.cmake Makefile"
alias x=xmake
alias xr="xmake run"
alias xc="xmake clean"
export QT_SELECT=5 # Tell to QTchooser that we want to use QT5

# PHP + Laravel + Pest + NPM + Cypress aliases
alias p="prjs"
alias pest="vendor/bin/pest --colors=always"
alias pw="pest --watch --filter" # Pest watch with filter
# alias p="pest -p"
alias pe="pest"
alias art=./artisan
alias arl="php artisan route:list --sort=domain"
alias amfs="php artisan migrate:fresh --seed"
alias nrw="npm run watch"
alias nrd="npm run dev"
alias nrb="npm run build"
alias nrs="npm run start"
alias nr="npm run"
alias pn="pnpm"
alias pnb="pnpm build"
alias pnt="pnpm test"
alias cyo="export NODE_OPTIONS=--openssl-legacy-provider && npx cypress open --config defaultCommandTimeout=1500"

## Kitty related
alias icat="kitty +kitten icat" # support untransformed image print in terminal

## Java related things
# Help: to configure the openjdk version to use (17 or 21 now), run "sudo alternatives --config java".
# To use OpenJDK 21 (or the latest version): sudo dnf install java-latest-openjdk
# https://jan-j-barrera.medium.com/managing-multiple-java-versions-in-linux-126801622017
### Inform Maven to use the java version that reply to the "java" command
# export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which java)))))
alias gr="gradle"
alias grr="gradle run"
alias grb="gradle build"

# Remove limit to bash history
# https://superuser.com/questions/137438/how-to-unlimited-bash-shell-history
export HISTFILESIZE=
export HISTSIZE=

. "$HOME/.cargo/env"

alias d="docker"
alias dc="docker compose"
# Docker Compose Enter - easy command to enter running containers
dce() {
    sudo -v
    line=$(dc ps --format "table {{.ID}}\t{{.Service}}\t{{.Ports}}" | sed '1d' | fzf)
    # id=$(echo "$line" | cut -d " " -f1)
    service=$(echo "$line" | cut -d " " -f4)
    echo "Entering $service"
    dc exec "$service" sh
}

# Shell wrapper around the docker image named "ctp" and locally built from https://github.com/samuelroland/ctp
function ctp() {
    # We need to create the output file before the container to set permission to $USER instead of root
    if test $# -ge 3; then
        name=$3
        if ! echo $name | grep .puml; then
            name="$name.puml"
        fi
        touch "$name"
    fi

    docker run -v .:/code ctp $@
}

## Shortcut to easily hash a value in MD5
function md5() {
    echo -n "$1" | md5sum
    # -n is used to not insert an endline
}

# TODO: fix this not very functionnal bash function
deleteoldkernels() {
    set -e # exit at the first error
    if [ -n "$1" ]; then
        versionPrefix=$1
        echo "Are you sure about deleting the following kernel packages ?"
        rpm -qa | grep -P ^kernel.*-$versionPrefix || (echo "No kernel package found with version $versionPrefix. Quitting the script...")
        # Help from: https://stackoverflow.com/a/226724
        read -p "Confirm the action [y/n]: " yn
        case $yn in
        [Yy]) sudo dnf remove $(dnf repoquery "kernel*$versionPrefix*" --installonly -q) ;;
        [Nn]) echo "Script stopped" ;;
        *) echo "Please answer y for yes or n for no." ;;
        esac

    else
        echo "No kernel version provided as first parameter..."
        echo "Availables kernels:"
        rpm -qa | grep kernel-core
    fi

}

# Customized "git add" (ga) with an easy way to choose multiple files to add (select with tab) and inspect them directly
ga() {
    git diff --name-only | fzf -m --ansi --preview "git diff --color=always -- {-1}" | xargs -d $'\n' sh -c 'for arg do git add "$arg"; echo "File staged: $arg"; done' _
}

## Run cmake project (find values under ./build/subdirectory/subdirectoryexecutable or ./build/projectexecutable )
run() {
    subdirectory=$(cat CMakeLists.txt | grep -Po "^add_subdirectory\(.*\)" | cut -d '(' -f 2 | cut -d ')' -f 1)
    if [ -n "$subdirectory" ]; then
        ./build/$subdirectory/$subdirectory
    else
        project=$(cat CMakeLists.txt | grep -Po "project\(.*\)" | cut -d '(' -f 2 | cut -d ')' -f 1)
        ./build/$project
    fi
}

# Enable commit signing for Github account (currently not supported in Gitui...)
engithubsign() {
    git config user.signinkey 4825D0F6570C7A31
    git config commit.gpgsign true
}

# Functions
## Display latest modified files with a few exclusions (useful to find dotfiles)
lsmf() {
    minutes=2
    if [ -n "$1" ]; then
        minutes=$1
    fi
    exclusionRegex="\.cache|\.mozilla|\.(log|so|bin)$|(leveldb)$|/\.git\/|\/VSCodium\/User\/(History|workspaceStorage|globalStorage)|\/GPUCache"

    find . -mmin "-$minutes" |                   # find all files in the current folder changed in the last x minutes
        grep -v -P "$exclusionRegex" |           # filter the list to remove unrelevant files
        xargs ls -lthd --color |                 # list the files and folder in list ordered by modification date
        awk -F" " '{print $6, $7, $8, $5, $9}' | # select relevant columns (and remove permissions, owner, group, ...)
        column -t                                # reformat output with a table format
}

print() {
    message=$1
    color=$2

    if [ -z $color ]; then
        echo $message
    else
        declare -A colors
        colors[red]=31
        colors[green]=32
        colors[yellow]=33
        colors[blue]=34
        colors[magenta]=35
        colors[gray]=30
        colors[cyan]=36

        printf "\033[33;"
        printf ${colors[$color]}
        printf "m$message\033[0m"
        printf "\n"
    fi
}

gitopen() {
    echo "todo"
}

# Replace a few words by something else in a text file, using sed
replace() {
    if [ "$1" != 'strict' ] && [ "$1" != 'lax' ]; then
        echo "failed. first parameter must be strict or lax..."
    else
        if [ "$1" == 'strict' ]; then # strict mode means the match is on the full line
            sed -i "s/^$2$/$3/" "$4"
        else # else it's lax -> it means it needs to start with what we give and it replace the full line
            sed -i "s/^$2.*/$3/" "$4"
        fi
    fi
}

laraup() {
    ls | grep composer.json # a way to check if the current folder is a laravel project...
    if [[ $? -eq 0 ]]; then
        print ">> 1. Installing Composer and NPM dependencies:" magenta
        composer i
        npm i
        print ">> 2. Create .env file and generate app key:" magenta
        cp .env.example .env
        php artisan key:generate

        ## Create MySQL database (and setup in env)
        ### Help: https://stackoverflow.com/questions/1371261/get-current-directory-name-without-full-path-in-a-bash-script
        print ">> 3. Create MySQL database and setup .env values:" magenta
        db=$(basename "$PWD")
        valet db:create "$db"
        echo "Created db $db"
        replace lax "DB_DATABASE=" "DB_DATABASE=$db" .env
        replace lax "DB_USERNAME=" "DB_USERNAME=root" .env
        replace lax "DB_PASSWORD=" "DB_PASSWORD=" .env
        php artisan migrate:fresh --seed

        print ">> 4. Storage symlink, npm prod build, valet link and firefox opening:" magenta
        php artisan storage:link
        npm run prod
        valet link
        firefox "http://$db.test"
    else
        print "No composer.json found, is it really a Laravel project ??" red
    fi
}

# From https://superuser.com/questions/1552481/how-to-crop-svg
# Documented in AFO.
cropsvg() {
    inkscape --actions "select-all;fit-canvas-to-selection" --export-overwrite "$(pwd)/$1"
}

## Rust related things
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

# tabtab source for packages
# uninstall by removing these lines
[ -f ~/.config/tabtab/bash/__tabtab.bash ] && . ~/.config/tabtab/bash/__tabtab.bash || true

# NVM config
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

# pnpm
export PNPM_HOME="/home/sam/.local/share/pnpm"
case ":$PATH:" in
*":$PNPM_HOME:"*) ;;
*) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

# Measure text length
len() {
    text="$@"
    echo "length is ${#text}"
}

# https://github.com/ajeetdsouza/zoxide
eval "$(zoxide init bash)"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/sam/.sdkman"
[[ -s "/home/sam/.sdkman/bin/sdkman-init.sh" ]] && source "/home/sam/.sdkman/bin/sdkman-init.sh"
