# Command to "Update All" packages from all package managers
function upa
    step "DNF packages"
    sudo dnf5 update -y
    step "Flatpak packages"
    flatpak update -y
    # step "Snap packages"
    # sudo snap refresh
    step "NPM global packages"
    sudo npm -g update
    #step "Composer global packages"
    # TODO: for some reason this clears the sudo cache...
    #composer global update
    step "TLDR cache update"
    command tldr --update
    step "Yazi plugins"
    ya pack -u
    step "Neovim plugins via lazy.nvim"
    nvim --headless "+Lazy! sync" +qa
    step "Mason language servers update"
    nvim --headless +"MasonUpdate" +q
    step "Go tools"
    go-global-update
    step "Cargo crates"
    cargo install-update -a --locked

    step "Pip packages"
    # That's annoying that pip doesn't have an update command
    # Given from https://stackoverflow.com/questions/2720014/how-to-upgrade-all-python-packages-with-pip
    # TODO remove python usage and use jq instead !
    pip --disable-pip-version-check list --outdated --format=json | python -c "import json, sys; print('\n'.join([x['name'] for x in json.load(sys.stdin)]))" | xargs -n1 pip install -U
end
