# Try to optimize excalidraw export SVG files
# First very dumb version that is starting to work...
# It requires to have svgo installed globally (npm install -g svgo)

# Optimize inkscape logos, note for further optimisations
# remove sodipodi:docname="logo-unpath.svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
# seems outside of svg standard <sodipodi:namedview id="namedview1" pagecolor="#ffffff" bordercolor="#000000" borderopacity=".25" inkscape:showpageshadow="2" inkscape:pageopacity="0" inkscape:pagecheckerboard="0" inkscape:deskcolor="#d1d1d1" inkscape:document-units="mm"/>
#inside style -> font-family:'Fira Code';-inkscape-font-specification:'Fira Code, Normal';
#
function opti
    if test (count $argv) -lt 1
        echo "Error: you need to provide the file to optimize"
        return
    end
    set -l in $argv[1]

    if ! test -f "$in"
        echo "Error: file not found"
        return
    end
    set -l out (string split ".svg" "$in")[1]".opti.svg"

    # Hacky fix to remove numbers pathify issues (they just disappear and take a bigger white space...)
    # We want to remove fallbacks fonts so we want to replace font-family="Excalifont, Segoe UI Emoji" to font-family="Excalifont"
    # TODO: we might want to this for other fonts from Excalidraw (Virgil, Cascadia, others...) in case Excalifont is not the only one used...
    sed -i 's/font-family="Excalifont, Segoe UI Emoji"/font-family="Excalifont"/g' $in
    sed -i 's/font-family="Helvetica, Segoe UI Emoji"/font-family="Helvetica"/g' $in

    # svg screenshots have mark-for so we can skip pathifying for them
    grep mark-for "$in" &>/dev/null
    if test $status != 0 && [ "$argv[2]" != basic ]
        step "Export to path in Inkscape"
        ## Use inkscape to convert all the text to path so we don't depend on fonts being installed
        flatpak run org.inkscape.Inkscape --export-filename="$PWD/$out" --export-text-to-path "$PWD/$in" &>/dev/null
    else
        cp "$in" "$out" # rename to avoid changing next command
    end
    ## Run various svg optimisations configured in mentionned config file
    step "SVGO optimisations"
    svgo "$out" --config ~/.config/svgo.excalidraw.js

    echo Done optimizing $in, resulted in $out

end
