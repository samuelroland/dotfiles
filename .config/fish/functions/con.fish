# con - Edit some regular CONfigurations files, most of them are versionned in dotfiles
function con
    set TMP /tmp/config-file-tmp
    pushd $HOME
    set list (git ls-tree -r HEAD --name-only | grep -P "\.(toml|ron|yml|yaml|json|conf)\$|\.git.*" | grep -v "/nvim")
    begin
        printf %s\n $list
        fd . .config/fish/functions/ --no-ignore # because some of them might be not versionned
        ls .config/fish/*.fish
        echo "local.sh"

        ## VSCode config
        fd -e json . .config/Code/User/ --no-ignore | grep -vP "History|workspace|entries|storage|globalStorage"
    end | fzf --height=~100% >$TMP
    set file (cat $TMP)
    if test -z "$file"
        popd
        return
    end
    set file "$HOME/$file"
    if test -f "$file"
        $EDITOR $file
        echo Edited $file
    else
        color red "Error: file $file doesn't exist"
    end
    popd
end
