# puml - Convert PlantUML schema to PNG/SVG or other supported formats
# Author: Samuel Roland
# Synopsis: puml [PlantUML file] [export format]
# Examples: puml # auto detect .puml files
#           puml diagram.puml # export given file in SVG
#           puml diagram.puml png # export given file in PNG
# License: MIT
# Notice: Copyright (c) 2024 Samuel Roland
# Source: https://codeberg.org/samuelroland/productivity/src/branch/main/HEIG/tools

# TODO: support/document changing the server
set PUML_SERVER http://localhost:5001
set format svg
function puml
    if ! test -z $argv[2]
        set format $argv[2]
        # TODO: validate the format before injecting it in the url
    end
    if ! test -z $argv[1]
        if test -f $argv[1]
            set schema $argv[1]
        else
            color red "File $argv[1] not found..."
        end
    else
        set puml_list (fd -e puml -e plantuml)
        # TODO: continue here with fzf and co to select puml or choose the only one
    end
    set basename (basename -s .puml $schema)
    set basename (basename -s .plantuml $basename)
    set image_name $basename.$format

    color blue "Sending the schema for $format generation from $schema to $image_name..."
    cat $schema | curl --silent --show-error --fail --data-binary @- "$PUML_SERVER/$format/" --output - >$image_name

    if test $status -ne 0
        color red "Failed to convert $schema to $format"
        return
    end

    if command -q -v icat # if icat is found
        color cyan "Printing generated schema"
        icat --background=white $image_name
    else
        color cyan "Finished export to $image_name"
    end
end
