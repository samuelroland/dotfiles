function doc
    # Easily open any documentation page available in terminal
    # Support various source like TLDR, man pages, fish help, ...

    set -l tldr (tldr -o -l | sed -e "s/^/tldr: /")
    set -l man (man -k . -s 1 | sed -e "s/^/man: /")
    set -l ansible (ansible-doc -l -j | jq "." | sed -e "s/^/ansible-doc: /")

    set -l choice (begin
  printf %s\n "$tldr"
  printf %s\n $man
  printf %s\n $ansible
end | fzf)

    echo Documentation found for $choice
end
