# rev - Easily review commits in a nice TUI with words level diffing
# Requires git delta and diffnav

function rev
    # Select a commit 
    set hash (git log --pretty=format:'%C(bold yellow)%h %aN %C(green)%s' | fzf |  cut -d " " -f 1)
    # Open diff until this commit (included) and open it in diffnav !
    if ! test -z "$hash"

        # Review a single commit
        if [ "$argv[1]" = one ]
            git diff $hash~ $hash | diffnav
        else # or review all commits until given one (included)
            git diff $hash~1 | diffnav
        end
    end
    # Enjoy !
end
