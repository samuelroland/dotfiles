# Couchbase watch -> Run a query with Couchbase shell on file save
# Requires the couchbase shell: https://github.com/couchbaselabs/couchbase-shell
# STATUS: very drafty
# TODO: refactor with generic watcher
function cbwa
    set -g file $argv[1]
    function run
        set query (cat $file | sed "s/--.*//g" | grep -vP "^\s*\$")
        if test -z "$query"
            return
        end
        clear
        color blue "Running query with cbsh"
        printf %s\n $query | bat -l sql -p
        cbsh -c "query '$query'"
        # impossible to preserve the colors if we want to remove these lines...
        # &| grep -vP "^\$|configure tls to disable this warning|inotify_rm_watch"
    end

    echo watching: $file
    run
    fish -c "fswatch --event Updated -b -e \".\*\" -i \"\\.sql\" $file" &| while read --line >/dev/null
        sleep 0.1
        run
    end
end
