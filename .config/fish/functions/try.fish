## Try something in a docker container, use an existing one or create a fresh one !
set images fedora ubuntu custom-fedora # list of available images for new containers
# TODO: improve to support mounting volumes easily
# TODO: support seeing last modified container
function try
    set NEWOPTION "Start a new container"
    set existings (docker ps -a --format "{{.ID}}\t{{.Names}}\t{{.Status}}"  | awk '{print $2}') $NEWOPTION
    set choice (printf %s\n $existings | fzf --height=~100%)
    # Run an existing container
    if [ $choice != $NEWOPTION ]
        set existingId (echo $choice | cut -d " " -f4)
        if string length $existingId != 0
            docker container start -i $existingId
        end
    else # Run a new known image
        set id (printf %s\n $images | fzf --height=~100%)
        read -P "Give a name to the container: " name
        set name (string replace -a " " "-" (string trim $name))
        docker run --network host --name "$name" -it "$id"
    end
end
