# rclo - Clone a git repos with remote repos listing support
# STATUS: very experimental and not very used for now
function rclo
    set -l url (curl https://codeberg.org/api/v1/users/samuelroland/repos -s | jq ".[].ssh_url" -r | fzf --height=~100%)
    if ! test -z "$url"
        set -l folder (string sub -e -4 (string match  -r '[A-z_-]*\.git' "$url"))
        echo Cloning (color blue "$url") into (color red "$folder")
        git clone "$url"
        if ! test -f "$folder"
            echo Jumped into (color red "$folder") ":) "
            cd "$folder"
        end
    end
end
