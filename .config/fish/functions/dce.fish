# dce - Docker Compose Enter - easily enter into containers started with docker compose
# Author: Samuel Roland
# Synopsis: TODO
# Examples: TODO # why
#           TODO # why
# License: MIT
# Notice: Copyright (c) 2024 Samuel Roland
# Source: https://codeberg.org/samuelroland/productivity/src/branch/main/HEIG/tools

# TODO: detect standard compose files or throw an error when not found
# TODO: reduce duplication if possible ?
# TODO: should we rename it to de or doe like docker enter ? should we make it general to allow entering in any container via id instead of service ?
# TODO: should we auto retry bash or sh if it fails

function dce
    set -l target

    # dc -f simple-prod.yml exec website sh
    if ! test -z "$argv[1]"
        set -l line (docker compose -f $argv[1] ps --format 'table {{.ID}}\t{{.Service}}\t{{.Ports}}' | sed '1d' | fzf)
        set -l service (echo "$line" | cut -d " " -f4)
        echo "Entering $service"
        docker compose -f $argv[1] exec "$service" sh
    else
        set -l line (docker compose ps --format 'table {{.ID}}\t{{.Service}}\t{{.Ports}}' | sed '1d' | fzf)
        set -l service (echo "$line" | cut -d " " -f4)
        echo "Entering $service"
        docker compose exec "$service" sh
    end
end
