# Asciinema - terminal recordings
alias an asciinema
alias anp "asciinema play"
# Cleaner terminal during private mode to have cleaner recordings
if test -n "$fish_private_mode"
    # Clean prompt
    function fish_greeting
    end
    function fish_prompt
        # TODO: add more colors ?
        echo (basename $PWD)'> '
    end
    function fish_mode_prompt
    end
end
# Start a recording
function rec
    read -P "name: " subject
    set filename $subject.cast
    set clean $subject.clean.cast
    asciinema rec $filename -c 'fish --private'
    echo "Cleaning up $filename file into $clean"
    # Clean short path like "~/H/d/S/something" to "something" even they are not displayed
    # TODO: should we also string replace -a "$PWD" (basename $PWD)  ??
    cat $filename | string replace -a (prompt_pwd) (basename $PWD) | string replace -a (echo 'file://'(hostname)$PWD) (basename $PWD) >$clean
end

# Only generate all these functions and aliases if we are in an interactive shell
if status is-interactive
    # Enable Vi key bindings :)
    fish_vi_key_bindings

    # Python 2.7 used by SO3 debugging in SYE
    set -ga LD_LIBRARY_PATH /opt/python/2.7/lib

    if test -f /etc/grc.fish
        source /etc/grc.fish
    end

    bind \cz 'fg 2>/dev/null; commandline -f repaint'

    if test -f ~/local.sh
        source ~/local.sh
    end

    # HEIG-VD syncing files
    function git_pull
        pushd $argv[1]
        git pull
        popd
    end

    function hsy
        step "Pulling files from Moodle"
        edu-sync-cli sync
        step "Pulling PLP"
        git_pull /home/sam/HEIG/year3/PLP/material
        step "Pulling SDR"
        git_pull /home/sam/HEIG/year3/SDR/cours
        step "Pulling AMT"
        git_pull /home/sam/HEIG/year3/AMT/cours
    end

    # Fish history has a high limit of 262,144 *unique* commands, it cannot be changed and it is probably high enough to not actually consider to backup it
    # https://github.com/fish-shell/fish-shell/issues/2674

    ## Very specific functions probably useful to only me

    ## Easily Edit my Ansible Playbook + generate docs
    set -g SETUP_REPOS_PATH ~/code/setup
    function eap
        pushd $SETUP_REPOS_PATH
        set -l file (fd -e yml -e md -e fish | fzf)
        if test -f "$file"
            echo "Editing $file..."
            $EDITOR "$SETUP_REPOS_PATH/$file"

            # If there are any change, generate docs again
            if ! git diff --exit-code >/dev/null
                ./docs.fish
            else
                echo Skipped generating docs...
            end
        end
        popd
    end

    function giftomp4
        if string length "$argv[1]" != 0 >/dev/null
            set -l newname (basename -s .gif "$argv[1]").mp4
            ffmpeg -i $argv[1] -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" "$newname"
        else
            color red Give the name of a .gif file as first argument...
        end
    end

    ## Browsing aliases

    ## Quickly create a new document with the current date as prefix
    abbr --add nnow nvim (date -u +%Y-%m-%d)-

    # Open any PDF in $HOME, do not read .gitignore, filter lines by word "cours"
    # to only include relevant PDF, use fzf to select and open with xdg-open with quotes around the filename
    alias cours 'fd -e pdf --no-ignore . $HOME | grep cours | fzf | xargs -I _ xdg-open "_"'

    ## Misc tools
    alias spp spotify_player
    alias dlsound "yt-dlp -x --audio-format mp3"

    # DEV
    # Load the SSH private key as Gitui doesn't read .ssh/config
    ssh-add ~/.ssh/id_rsa_git &>/dev/null

    ## Haskell
    # More on https://codeberg.org/samuelroland/productivity/src/branch/main/HEIG/PLP/tips.md
    function ghcir
        rush "ghci $argv[2]" "ghci> " $argv[1]
    end

    alias rmcmake "rm -rf build/ CMakeFiles/ _deps/ CMakeCache.txt cmake_install.cmake Makefile"
    alias x xmake
    alias xr "xmake run"
    alias xc "xmake clean"
    set -g QT_SELECT 5 # Tell to QTchooser that we want to use QT5

    # PHP + Laravel + Pest + NPM + Cypress aliases
    alias pest "vendor/bin/pest --colors always"
    alias pw "pest --watch --filter" # Pest watch with filter
    # alias p "pest -p"
    alias pe pest
    alias art ./artisan
    alias arl "php artisan route:list --sort domain"
    alias amfs "php artisan migrate:fresh --seed"
    alias cyo "export NODE_OPTIONS --openssl-legacy-provider && npx cypress open --config defaultCommandTimeout 1500"

    ## Java related things
    # Help: to configure the openjdk version to use (17 or 21 now), run "sudo alternatives --config java".
    # To use OpenJDK 21 (or the latest version): sudo dnf install java-latest-openjdk
    # https://jan-j-barrera.medium.com/managing-multiple-java-versions-in-linux-126801622017
    ### Inform Maven to use the java version that reply to the "java" command
    # export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which java)))))

    ## Rustlings show diff between my answer and solution via git delta
    ## -> just easier than clicking on solution link in Rustlings and 
    ## manually comparing what has changed
    function lingsdiff
        set exo_path (fd -e rs --base-directory exercises/ | fzf)
        delta exercises/$exo_path solutions/$exo_path
    end

    ## Easily create new fish functions with ready header
    function new_fish_func
        if test (count $argv) -ne 1
            color red "Error: should give new function name as first arg"
            return
        end

        set name $argv[1]
        set file ~/.config/fish/functions/$name.fish

        if test -f $file
            color red "Fish function $name already exists !"
            return
        end

        set header """# tool - description
# Author: Samuel Roland
# Synopsis: TODO
# Examples: TODO # why
#           TODO # why
# License: MIT
# Notice: Copyright (c) 2025 Samuel Roland
# Source: https://codeberg.org/samuelroland/productivity/src/branch/main/HEIG/tools
        """

        echo "$header" >$file
        echo -e "function $name\n\nend" >>$file
        nvim $file
    end

    ## Easily migrate fish function from local folder to productivity repository for publishing
    function migrate_fish_func
        set fish_base ~/.config/fish/functions/
        set productivity ~/code/productivity/HEIG/tools/
        set filename (ls $fish_base | grep -v -- "->" | fzf)
        if test -f $fish_base/$filename
            if test -f $productivity/$filename
                color red "Fish function already exists in productivity repository"
                return
            end
            mv $fish_base/$filename $productivity/$filename
            ln -s $productivity/$filename $fish_base/$filename
            cd $productivity
            git add $filename
            gitui
        else
            color red "Fish function doesn't exist"
        end
    end

    # Include shared abstractions from shared.fish
    if test -f ~/.config/fish/shared.fish
        source ~/.config/fish/shared.fish
    end

    # disable dead code and unused variables warnings
    # enable an alternative faster linker called wild
    # cargo install --locked --bin wild --git https://github.com/davidlattimore/wild.git wild
    # https://github.com/davidlattimore/wild.git
    # export RUSTFLAGS="$RUSTFLAGS -A dead_code -A unreachable_code -A unused_imports -A unused_variables -Clinker=clang -Clink-args=--ld-path=wild"
end
