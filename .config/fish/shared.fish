# Shared fish configurations for my Fedora setup
# Contains a lot of useful aliases
# Author: Samuel Roland
# License: MIT
# Notice: Copyright (c) 2025 Samuel Roland
# Source: https://codeberg.org/samuelroland/dotfiles/src/branch/main/.config/fish/shared.fish

# Aliases and functions useful to be shared !

# Only generate all these functions and aliases if we are in an interactive shell
if status is-interactive
    ## Environment variables - those should be overridable !
    set -g SCR_TMP_FOLDER ~/code/tmp

    ## UTILS
    # Show a script step in color
    function step
        color green ">>>" $argv
    end
    # Print a text in a given color - without ending line
    # Usage: color <color> some text
    # See more in https://fishshell.com/docs/current/cmds/set_color.html
    function color
        set_color $argv[1] -o # set color and bold mode
        echo $argv[2..-1]
        set_color normal
    end

    # Disable the fish greeting on Fish start
    function fish_greeting
    end

    # Self explanatory - Kill a process by port (TCP)
    ## https://stackoverflow.com/questions/11583562/how-to-kill-a-process-running-on-particular-port-in-linux
    function killbyport
        set_color red && echo "Killing process on port $argv[1]" && set_color normal
        sudo fuser -k "$argv[1]/tcp"
    end

    # Add colors to man page ! Use bat as pager.
    export MANPAGER="sh -c 'col -bx | bat --theme=\"Monokai Extended Origin\" -l man -p'"
    export GROFF_NO_SGR=1


    # See mvr.fish too
    alias mvp="mvn -T 1C package"
    alias mvpr="mvp && cls && java -jar target/*.jar"

    # Aliases
    # TODO: should we publish it via gnfit ?
    # PRoductivity Markdown document OPening in browser
    # Help to open one of my cheatsheet or document in my productivity repository
    function prop
        set choice (curl 'https://codeberg.org/api/v1/repos/samuelroland/productivity/git/trees/main?recursive=true' -s | jq -r '.tree[].path' | grep -vP "yazi/practice|.png" | grep -P "common-setup|.md" | fzf)
        set BASE_URL https://codeberg.org/samuelroland/productivity/src/branch/main
        xdg-open $BASE_URL/$choice
    end

    # Ansible related aliases and functions
    alias ad ansible-doc

    # Open Lazyvim configuration
    function elv
        set -l file (fd --no-ignore -e lua -e toml . ~/.config/nvim | fzf)
        $EDITOR "$file"
    end

    ## Pull all Git repositories in current folder
    function pullall
        set -l list (fd -t d --hidden --no-ignore --max-depth 2 | grep -P "\.git/\$")
        for repos in $list
            echo $repos
            pushd (string replace -a ".git" "" $repos)
            git pull &
            popd
        end
    end

    ## Browsing aliases

    # Yazi wrapper to easily change cwd to where Yazi left on exit
    # Copy pasted from https://yazi-rs.github.io/docs/quick-start#shell-wrapper
    function y
        set tmp (mktemp -t "yazi-cwd.XXXXXX")
        yazi $argv --cwd-file="$tmp"
        if set cwd (cat -- "$tmp"); and [ -n "$cwd" ]; and [ "$cwd" != "$PWD" ]
            cd -- "$cwd"
        end
        rm -f -- "$tmp"
    end
    alias ls eza # A better ls with colors
    alias l ls
    alias ll "ls -l"
    alias la "ls -la"
    alias rm 'rm -i'
    alias mv 'mv -i'

    # Change .... to cd ../../..
    # From https://fishshell.com/docs/current/interactive.html#abbreviations 
    function multicd
        echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
    end
    abbr --add dotdot --regex '^\.\.+$' --function multicd

    # Make sure TLDR runs in offline mode to not reload its cache (because we already make it up-to-date in upa function)
    alias tldr "tldr -o"
    # Open in default application (Dolphin if just a folder) with xdg-open
    alias o "open ."
    alias op xdg-open

    ## Create a new folder and jump inside
    function mk
        mkdir -p "$argv" && cd "$argv"
    end

    # Editor aliases
    alias n nvim
    alias co "codium ."
    alias testco 'ping 1.1.1.1 -c4 -v'
    set -g GIT_EDITOR nvim # so Gitui doesn't open nano...
    set -g EDITOR nvim
    set -g SCR_EDITOR "$EDITOR"

    # Fish related alias - easily Edit Fish config, Reload Fish config, search in history
    alias ef "$EDITOR ~/.config/fish/config.fish"
    alias eb "echo Error: This is the old bash alias, now changed to ef for Fish"
    alias rf "source ~/.config/fish/config.fish"
    alias rb "echo Error: This is the old bash alias, now changed to rf for Fish"

    # Fedora aliases
    alias listkernels "rpm -qa | grep kernel-core-"
    alias di "sudo dnf5 install -y"
    alias dr "sudo dnf5 remove"
    alias diskspace "sudo btrfs f usage /"

    ## Shortcut to easily hash a value in MD5
    function md5
        set -l out (echo -n "$argv" | md5sum) # -n because we don't want to not insert an endline
        set -l hash (string sub -l 32 "$out")
        echo The MD5 hash of litteral text "'$argv'" is: $hash
    end

    ## Show a nice git diff and export it to HTML - requires pip install ansi2html and a better differ like git-delta
    function gitdifftohtml
        if test (count $argv) -eq 2
            git diff --color "$argv[1]" | ansi2html >$argv[2].html
            # Note: to filter files you can run something like `git diff -- (fd -e yml)`
            open "$argv[2].html"
        else
            echo Provide the hash you want to diff from and the output file
        end
    end

    ## Misc tools
    alias spp spotify_player
    alias dlsound "yt-dlp -x --audio-format mp3"

    # DEV
    ## Git aliases
    alias g gitui
    alias ghd "gh dash"
    alias gco "git branch | fzf | xargs git checkout"
    alias st 'git status'
    alias log "git log --graph --pretty=format:'%C(green)%ai %d %C(blue)%H %C(green)%cn %C(bold yellow)%s %C(green)%b'"
    alias lst 'git ls-tree -r HEAD'
    alias gf "git pull"
    alias gp "git push"
    alias gr "cd (git rev-parse --show-toplevel)" # jump to repos root
    alias gitcancel "git reset HEAD~1"

    ## Show information about Git identity
    function gitid
        echo "Current identity is"
        echo "Email: $(git config user.email)"
        echo "Name: $(git config user.name)"
    end

    ## CMake missing build and run commands
    alias cmb "cmake . -Bbuild && cmake --build build/ -j 8"
    alias cbr "cmb && cmr" # build + run
    alias cmr "QT_QPA_PLATFORMTHEME=qt5ct fish -c 'cmr \$argv'"

    ## Fix incorrect origin
    function fixor
        if test "$argv[1]"
            git config remote.origin.url $argv[1]
        else
            echo "Current origin is " (color green (git config remote.origin.url))
            echo "To change it run " (color blue "fixor <new-origin-url>")
        end
    end

    # NPM and PNPM aliases
    alias nrp "npm run prod"
    alias nrw "npm run watch"
    alias nrd "npm run dev"
    alias nrb "npm run build"
    alias nrs "npm run start"
    alias nr "npm run"
    alias pn pnpm
    alias pnb "pnpm build"
    alias pnt "pnpm test"

    ## Kitty related
    alias icat "kitty +kitten icat" # support untransformed image print in terminal

    # Java + gradle
    alias gd gradle
    alias grr "gradle run"
    alias gb "gradle build"
    alias mvw "./mvnw"
    alias qur "java -jar target/quarkus-app/*.jar"
    alias qub "mvw package -Dquarkus.package.type=uber-jar"

    # TODO: publish via gnfit ?
    ## SDKMAN access and tools access from fish
    function sdk # Just a wrapper around the bash script
        bash -c "source '$HOME/.sdkman/bin/sdkman-init.sh'
        sdk $argv"
        fish_add_path (find ~/.sdkman/candidates/*/current/bin -maxdepth 0) # each installed tool must be added to the path
    end

    # Docker
    alias d docker
    alias dc "docker compose"
    # See dce.fish too !
    alias lzd lazydocker

    ## Rust related things
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
    fish_add_path ~/.cargo/bin
    alias ca cargo
    alias car "cargo run"
    alias cab "cargo build -q"
    alias catr "cargo nextest run"
    alias cawt "cargo watch -x 'nextest run' -c"
    alias ct "cargo nextest run"

    # Setup of zoxide commands like z and zi
    # https://github.com/ajeetdsouza/zoxide
    zoxide init fish | source
end
