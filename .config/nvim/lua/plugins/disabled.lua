-- Disabled plugins

return {
  { "mrcjkb/rustaceanvim", enabled = false },
  -- { "mfussenegger/nvim-jdtls", enabled = false }, -- "You cannot use nvim-java alongside nvim-jdtls. So remove nvim-jdtls before installing this"
  { "DavidAnson/markdownlint-cli2", enabled = false },
  { "mfussenegger/nvim-lint", enabled = false },
}
