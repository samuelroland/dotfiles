return {
  "tadmccorkle/markdown.nvim",
  name = "bettermarkdown.nvim",
  ft = "markdown", -- or 'event = "VeryLazy"'
  opts = {
    link = {
      paste = {
        enable = true, -- whether to convert URLs to links on paste
      },
    },
  },
  on_attach = function(bufnr)
    local function toggle(key)
      return "<Esc>gv<Cmd>lua require'markdown.inline'" .. ".toggle_emphasis_visual'" .. key .. "'<CR>"
    end

    -- vim.keymap.set("n", "<C-b>", toggle("b"), { buffer = bufnr })
    -- vim.keymap.set("n", "<C-i>", toggle("i"), { buffer = bufnr, remap = true })
  end,
}
