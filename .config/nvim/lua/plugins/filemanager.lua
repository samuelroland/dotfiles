-- Define Yazi as default file manager instead of Neotree
-- https://github.com/Rolv-Apneseth/tfm.nvim
return {
  {
    "rolv-apneseth/tfm.nvim",
    config = function()
      -- Set keymap so you can open the default terminal file manager (yazi)
      vim.api.nvim_set_keymap("n", "<leader>e", "", {
        desc = "Open file manager (yazi)",
        noremap = true,
        callback = require("tfm").open,
      })
    end,
  },
}
