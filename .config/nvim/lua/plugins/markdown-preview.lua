return {
  "iamcco/markdown-preview.nvim",
  cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
  ft = { "markdown" },
  build = function()
    vim.fn["mkdp#util#install"]()
  end,
  config = function()
    -- Not a good idea as it somehow replace the existing style
    -- vim.g.mkdp_markdown_css = "/home/sam/markdown-preview-utils.css"
    vim.g.mkdp_theme = "light"
  end,
}
