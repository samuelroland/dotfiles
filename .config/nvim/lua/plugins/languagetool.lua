if true then
  return {}
end

-- In addition to Mason LSP ltex-ls
return {
  {
    "barreiroleo/ltex_extra.nvim",
    ft = { "markdown", "tex" },
    dependencies = { "neovim/nvim-lspconfig" },
    -- yes, you can use the opts field, just I'm showing the setup explicitly
    config = function()
      require("ltex_extra").setup({
        {
          -- All options: https://valentjn.github.io/ltex/settings.html
          -- table <string> : languages for witch dictionaries will be loaded, e.g. { "es-AR", "en-US" }
          -- https://valentjn.github.io/ltex/supported-languages.html#natural-languages
          load_langs = { "en-GB", "fr" },
          -- boolean : whether to load dictionaries on startup
          -- init_check = true,
          -- string : relative or absolute path to store dictionaries
          -- e.g. subfolder in the project root or the current working directory: ".ltex"
          -- e.g. shared files for all projects:  vim.fn.expand("~") .. "/.local/share/ltex"
          -- path = "/home/sam/.config/nvim/data", -- project root or current working directory
          -- TODO: fix this ! maybe with https://valentjn.github.io/ltex/settings.html#ltexdictionary
          path = vim.fn.expand("~") .. "/.config/nvim/data",
        },
        server_opts = {
          -- capabilities = your_capabilities,
          -- on_attach = function(client, bufnr)
          --   -- your on_attach process
          -- end,
          settings = {
            ltex = {
              languageToolHttpServerUri = "http://localhost:5000/",
              additionalRules = { motherTongue = "fr" },
            },
          },
        },
      })
    end,
  },
}
