-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- From https://www.reddit.com/r/neovim/comments/197by0m/useful_nvim_keymaps_for_rustlings/
vim.keymap.set({ "n" }, "<leader>n", function()
  local cur = vim.fn.expand("%")
  local num = cur:sub(-4, -4)
  local next = cur:sub(1, -5) .. (num + 1) .. ".rs"
  if vim.fn.filereadable(next) == 1 then
    vim.cmd("bd")
    vim.cmd("edit " .. next)
  else
    print("All problems solved for this topic.")
  end
end)

-- Disable default arrow keymaps to force to learn of hjkl
vim.keymap.set({ "n" }, "<Left>", "<nop>", { remap = true })
vim.keymap.set({ "n" }, "<Right>", "<nop>", { remap = true })
vim.keymap.set({ "n" }, "<Down>", "<nop>", { remap = true })
vim.keymap.set({ "n" }, "<Up>", "<nop>", { remap = true })

vim.keymap.set({ "n" }, "<leader>o", "o<Esc>", { noremap = true })
vim.keymap.set({ "n" }, "<leader>O", "O<Esc>", { noremap = true })
vim.keymap.set({ "n" }, "<C-q>", ":q<CR>", { noremap = true })

-- Markdown styling
vim.keymap.set({ "n" }, "<leader>mb", "gsiwb", { remap = true, desc = "Toggle bold style" })
vim.keymap.set({ "v" }, "<leader>mb", "gsb", { remap = true, desc = "Toggle bold style" })
vim.keymap.set({ "n" }, "<leader>mi", "gsiwi", { remap = true, desc = "Toggle italic style" })
vim.keymap.set({ "v" }, "<leader>mi", "gsi", { remap = true, desc = "Toggle italic style" })
vim.keymap.set({ "n" }, "<leader>mm", "gsiwc", { remap = true, desc = "Toggle inline code style" }) -- m like mention
vim.keymap.set({ "v" }, "<leader>mm", "gsc", { remap = true, desc = "Toggle inline code style" }) -- m like mention
vim.keymap.set({ "n" }, "<leader>ml", "i<space>$$<Esc>i", { remap = true, desc = "Insert inline math equation" })
vim.keymap.set({ "n" }, "<leader>mL", "i$$<CR><CR>$$<Esc>ki", { remap = true, desc = "Insert block math equation" })
vim.keymap.set({ "v" }, "<leader>mL", "c$$<CR>$$<Esc>kp", { remap = true, desc = "Surround with block math equation" })
vim.keymap.set({ "n" }, "<leader>mc", "i```<Esc>o", { remap = true, desc = "Insert code block" }) -- c like code
vim.keymap.set({ "v" }, "<leader>mc", "c```<Esc>p", { remap = true, desc = "Surround with code block" }) -- c like code
vim.keymap.set(
  { "n" },
  "<leader>mp", -- p like picture/photo
  '0cf(<img src="<Esc>f)cl" height="400" /><Esc>lx',
  { remap = true, desc = "picture to <img> with height" }
)
vim.keymap.set(
  { "v" },
  "<leader>md", -- d like details
  "c<details><CR><Esc>O<summary>TODO<Esc><<pO<Esc>",
  { remap = true, desc = "Surround with <details> + <summary> block" }
)

-- Mark task as done
vim.keymap.set({ "n" }, "<leader>md", "0f[lrx", { remap = true, desc = "Mark markdown checkbox as done" })

-- Jump to next or previous errors
vim.keymap.set("n", "é", vim.diagnostic.goto_next, { desc = "Go to next diagnostic" })
vim.keymap.set("n", "à", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic" })

-- Testing
vim.keymap.set("n", "<leader>r", ":ScratchSplit<CR>ggdG:read ! fish -c 'runq test.sql'<CR>")

-- Fixing
vim.keymap.set("n", "<leader>cf", "A;<Esc>")
