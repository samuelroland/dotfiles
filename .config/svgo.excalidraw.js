//SVG configuration to optimize SVG files before we commit and deploy them.
//Optimize SVG of Excalidraw exported diagrams

module.exports = {
  plugins: [
    // "reusePaths", remove too much stuff
    "removeMetadata",
    "mergePaths",
    "convertPathData",
    {
      name: "cleanupNumericValues",
      params: {
        floatPrecision: 2,
        leadingZero: true,
        defaultPx: true,
      },
    },
    {
      name: "cleanupListOfValues",
      params: {
        floatPrecision: 2,
        leadingZero: true,
        defaultPx: true,
      },
    },

    "convertTransform",
    "removeHiddenElems",
    "removeComments",
    {
      name: "removeElementsByAttr",
      params: {
        id: [],
        class: ["style-fonts"], //Excalidraw <style> with fonts
      },
    },
    {
      name: "addAttributesToSVGElement",
      params: {
        attributes: [{ "font-family": "font-family:Excalifont" }],
      },
    },
    {
      name: "removeAttributesBySelector",
      params: {
        selectors: [
          {
            selector: ":not(svg)",
            attributes: "font-family",
          },
          {
            selector: "[font-stretch='100%']",
            attributes: "font-stretch",
          },
          {
            selector: "[font-weight='400']",
            attributes: "font-weight",
          },
          {
            selector: "[aria-hidden='true']",
            attributes: "aria-hidden",
          },
          {
            selector: "[text-decoration='rgb(0, 0, 0)']",
            attributes: "text-decoration",
          },
          {
            selector: "[word-spacing='0']",
            attributes: "word-spacing",
          },
          {
            selector: "[color='#000']",
            attributes: "color",
          },
          {
            selector: "*",
            // attributes: ["aria-label", "aria-owns" /*"id"*/, , "mask"], // for logo: do not remove id !
            attributes: ["aria-label", "aria-owns", "id", "mask"], // for excalidraw
          },
        ],
      },
    },
  ],
};
