-- Based on the great configurations made by Jess Archer who releases her dotfiles under MIT:
-- https://github.com/jessarcher/dotfiles
-- I discovered them with her Laracasts course:
-- 'Neovim as a PHP and JavaScript IDE' https://laracasts.com/series/neovim-as-a-php-ide


--
--                            ⢸⣦⡈⠻⣿⣿⣿⣶⣄
--                            ⢸⣿⣿⣦⡈⠻⣿⣿⣿⣷⣄
--                      ⣀⣀⣀⣀⣀⣀⣼⣿⣿⣿⣿ ⠈⠻⣿⣿⣿⣷⣄
--                      ⠈⠻⣿⣿⣿⣿⣿⡿⠿⠛⠁   ⠈⠻⢿⣿⣿⣷⣄
--
-- Personal Neovim configuration of Jess Archer <jess@jessarcher.com>

require('user.plugins')
require('user.options')
require('user.keymaps')
require('user.misc')

-- vim: nospell
