-- Based on the great configurations made by Jess Archer who releases her dotfiles under MIT:
-- https://github.com/jessarcher/dotfiles
-- I discovered them with her Laracasts course:
-- 'Neovim as a PHP and JavaScript IDE' https://laracasts.com/series/neovim-as-a-php-ide


vim.keymap.set('n', '<Leader>tn', ':TestNearest<CR>')
vim.keymap.set('n', '<Leader>tf', ':TestFile<CR>')
vim.keymap.set('n', '<Leader>ts', ':TestSuite<CR>')
vim.keymap.set('n', '<Leader>tl', ':TestLast<CR>')
vim.keymap.set('n', '<Leader>tv', ':TestVisit<CR>')

vim.cmd([[
  let test#php#phpunit#options = '--colors=always'
  let test#php#pest#options = '--colors=always'
  let test#java#maventest#options = '-B -Dstyle.color=always'
  let test#javascript#runner = 'vitest'

  function! FloatermStrategy(cmd)
    execute 'silent FloatermSend q'
    execute 'silent FloatermKill'
    execute 'FloatermNew! --autoclose=0 '.a:cmd.''
  endfunction

  let g:test#custom_strategies = {'floaterm': function('FloatermStrategy')}
  let g:test#strategy = 'floaterm'
]])
