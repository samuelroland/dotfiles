-- Based on the great configurations made by Jess Archer who releases her dotfiles under MIT:
-- https://github.com/jessarcher/dotfiles
-- I discovered them with her Laracasts course:
-- 'Neovim as a PHP and JavaScript IDE' https://laracasts.com/series/neovim-as-a-php-ide


vim.g.floaterm_width = 0.8
vim.g.floaterm_height = 0.8
vim.keymap.set('n', '<F1>', ':FloatermToggle<CR>')
vim.keymap.set('t', '<F1>', '<C-\\><C-n>:FloatermToggle<CR>')
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>:FloatermHide<CR>')
vim.keymap.set('n', "<leader>b", ':FloatermNew! --cwd=<buffer> --disposable qb && exit || exit<CR>')
